# Free room finder at University of Sydney

## Introduction
The University of Sydney has a free room website which has all the data of the scheduled classes for a period of time.
This is data can be used to find exactly which rooms are free and the best room for the individual who requested it.
The best room can be defined by multiple metrics:

* Minimise the walking distance
* Minimise the number of room changes between hours
* Increase proximity to shops/cafes
* Minimise the total walking distance of a group of individuals

## Ideas/Steps
1. Generate free rooms using API request and a simple gui
    * Gui contains - time selector (thats it)
    * BeatifulSoup to scrape data
2. Generate shortest path between rooms
    * Find said location of room
    * Work backwards and generate shortest cumulative distance between room
    * Access users location and find the best sequence of rooms
3. Update GUI to show the map with the location of the rooms
4. Generate a DATABASE by scraping all the free rooms for the rest of semester
    * How will we deal with if the rooms are updated/get booked...
        * Possibly after using the database to search for rooms, check the rooms are free manually, if not free then do the basic search not using the database...
5. Create a google calendar event and export it