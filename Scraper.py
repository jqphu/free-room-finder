import asyncio
import concurrent.futures
import requests
from bs4 import BeautifulSoup
import queue
import re
import functools

def make_url(venueId) :
    url = 'https://web.timetable.usyd.edu.au/venuebookings/venueCalendar.jsp?venueId={}&mode=Bookings'.format(venueId)
    print(url)
    return url

def get_classes_for_venIds(venueIds) :
    loop = asyncio.get_event_loop()
    roomQ = queue.Queue()
    loop.run_until_complete(main(venueIds, roomQ))
    return roomQ

async def main(venueIds, roomQ):
    with concurrent.futures.ThreadPoolExecutor(max_workers=64) as executor:

            venueId = 409
            loop = asyncio.get_event_loop()
            futures = [
                    loop.run_in_executor(
                    executor,
                    functools.partial(requests.get, make_url(venueId)                 ,
                    verify=False)

                )
                for venueId in venueIds
            ]
            for response in await asyncio.gather(*futures):

                try :
                    venueId = re.findall(r'venueId=(.*)&',response.url)[0]
                    soup = BeautifulSoup(response.text, 'html.parser')
                    tables = soup.find_all('table')
                    #print(venueId, file = o)

                    bookings = []

                    for t in tables :
                        if t.tr.th is not None and '<th>Booking #</th>' in str(t.tr.th):
                            rows = t.find_all('tr')
                            for row in rows[1:]:
                                elems = row.find_all('td')
                                bookingName = elems[1].a.text
                                day = elems[2].text
                                times = elems[3].text
                                bookings.append((day, bookingName, times))
                                #print(bookingName, day, times, file = o)

                    roomQ.put((venueId, bookings))
                except Exception as e:
                    print('GET Failed for url: ')
                    print(response.url)
                    print(e)
