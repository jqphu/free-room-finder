import requests
from flask import Flask, request
import sys
import math
from geopy import distance
import csv
import json
from Structures import Building
from Structures import Room
from Structures import Booking
from algo import *
from bs4 import BeautifulSoup
from flask_cors import CORS


# BNR lat and longitude
PNRLat = -33.890161899999
PNRLon = 151.1922707
CARSLAW_WEST_LAT =-33.888557
CARSLAW_WEST_LON = 151.190661 

from flask import Flask

app = Flask(__name__)
CORS(app)
def main():
    app.run(debug=True)

@app.route('/dataRequest',methods=['POST'])
def dataRequest():
    print('DATA REQUEST')

    get_buildings_dict()
    result = request.get_json()
    startTime = result['start_time']/1000
    pos = (result['lat'], result['lon'])
    retRoom = get_free_rooms(startTime, pos);
    return json.dumps(retRoom);
    
def allRooms(startTime, endTime, day, month):


    response = requests.get('https://web.timetable.usyd.edu.au/venuebookings/venueAvailDateTimeIndex.jsp?day={}&month={}&year=2018&startTime=-1&endTime=-1&startTimeAny={}&endTimeAny={}&domainId=1&buildingId=%A0&venueAvail=B'.format(day,
        month, startTime, endTime), verify=False)

    soup = BeautifulSoup(response.text, 'html.parser');

    table = soup.find('table', attrs={'width':'100%', 'border': '1'})

    listOfVenues = []
    for i in table.select('tr'):
        data = i.select('td')
        if data:
            venueLink = data[0].find('a').get('href')
            venueNumber = venueLink[venueLink.find('=') + 1:]
            listOfVenues.append(int(venueNumber))
            
    venueToBuilding = {}
    # Read in csv and store the venue code to building
    with open('venueToBuilding.csv', 'r') as csv_read_file:
        reader = csv.reader(csv_read_file)	
        for row in reader:
            venueToBuilding[row[0]] = row[1]

    
    buildingToLatLon = {}
    # Read in csv and store the long lat
    with open('buildingToLongLat.csv', 'r') as csv_read_file:
        reader = csv.reader(csv_read_file)	
        for row in reader:
            buildingToLatLon[row[0]] = [row[1], row[2]]

    distMin = math.inf

    # Store long lat in a string
    # Find min destination in matrix
    origin = (CARSLAW_WEST_LAT, CARSLAW_WEST_LON)

    venuesAndDistance = {} 

    # Find the closest venue!
    for venueCode in listOfVenues:
        if str(venueCode) in venueToBuilding:
            buildingCode = venueToBuilding[str(venueCode)]
            if buildingCode in buildingToLatLon:
                latLon = buildingToLatLon[buildingCode] 
                destination = (float(latLon[0]), float(latLon[1]))
                dist = distance.distance(origin, destination).km
                venuesAndDistance[venueCode] = {
                        'buildingCode': buildingCode,
                        'lat': latLon[0],
                        'lon': latLon[1],
                        'dist': dist,
                }

    return venuesAndDistance;


                    

        #https://maps.googleapis.com/maps/api/distancematrix/json?&origins=40.6655101,-73.89188969999998&destinations=40.6905615,-73.9976592%7C40.6905615%2C-73.9976592&key=AIzaSyDbdmFZNW9qzIZAIMsrVO-6XnYN_qNgYiw

    import pdb;
    pdb.set_trace()







    print('done')



    

if __name__ == "__main__":
    main()
