import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { TimepickerComponent } from './timepicker/timepicker.component';
import { AgmCoreModule } from '@agm/core';
import { HttpClientModule } from '@angular/common/http';
import {MatTableModule} from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material';

import { MatPaginatorModule }  from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatSortModule} from '@angular/material/sort';

@NgModule({
    declarations: [
        AppComponent,
        TimepickerComponent
    ],
    imports: [
        MatTableModule,
        MatFormFieldModule,
        MatSortModule,
        MatInputModule,
        MatPaginatorModule,
        BrowserModule,
        FormsModule,
        BrowserAnimationsModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyCObbJiajYr-Uz-s1lY0wQnLt3m85CCEQs'
        }),
        HttpClientModule,
    ],
    providers: [FormsModule],
    bootstrap: [AppComponent]
})
export class AppModule { }
