import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatPaginator, MatSort, MatTableDataSource} from '@angular/material';



@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

    private DATA_URL = "http://localhost:5000/dataRequest";
    private title = 'app';
    public pos;
    private lat: number = -33.888159;
    private lon: number = 151.190724;
    private centerlat;
    private centerlon;
    private zoom = 16;
    private result;
    private displayedColumns = ['name', 'dist', 'timeLeft', 'rank'];
    private dataSource = new MatTableDataSource();
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    private selectedRowIndex;
    private markerSelected = false;
    private newLat: number = -33.888159;
    private newLon: number = 151.190724;

    private applyFilter(filterValue: string) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource.filter = filterValue;
    }


    private markerMoved(e) {
        this.lat = e.coords.lat;
        this.lon = e.coords.lng;
        this.centerlat = this.lat;
        this.centerlon = this.lon;

        const nowEpoch =  (new Date('May 7, 2018 11:13:11')).getTime();
        
        const data = {
            lat: this.lat,
            lon: this.lon,
            start_time: nowEpoch
        }
        
        this._http.post(this.DATA_URL, data).subscribe( (response) => {
            console.log('RESULTS', response);
            this.result = JSON.parse(JSON.stringify(response));

            const DATA = [];

            // Every list
            for(let i = 0; i < this.result.length; ++i)
            {
                const tempData = {
                    name: this.result[i][0],
                    dist: this.result[i][1],
                    timeLeft: this.result[i][2],
                    lat: this.result[i][3][0],
                    lon: this.result[i][3][1],
                    rank: this.result[i][5],
                };
                DATA.push(tempData);
            }
            this.dataSource.data = (DATA);
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
        });


    }


    private mapProp = {}
        
    constructor(
        private _http: HttpClient,
    ) {}

    ngOnInit() {
        if(navigator.geolocation){
            navigator.geolocation.getCurrentPosition(position => {
                this.pos = position.coords;
                this.lat = this.pos.latitude;
                this.lon = this.pos.longitude;
                this.centerlat = this.lat;
                this.centerlon = this.lon;
                console.log(position.coords); 

                const nowEpoch =  (new Date('May 7, 2018 11:13:11')).getTime();

                const data = {
                    lat: this.lat,
                    lon: this.lon,
                    start_time: nowEpoch
                }

                this._http.post(this.DATA_URL, data).subscribe( (response) => {
                    console.log('RESULTS', response);
                    this.result = JSON.parse(JSON.stringify(response));

                    const DATA = [];

                    // Every list
                    for(let i = 0; i < this.result.length; ++i)
                    {
                        const tempData = {
                            name: this.result[i][0],
                            dist: this.result[i][1],
                            timeLeft: this.result[i][2],
                            lat: this.result[i][3][0],
                            lon: this.result[i][3][1],
                            rank: this.result[i][5],
                        };
                        DATA.push(tempData);
                    }
                    this.dataSource.data = (DATA);
                    this.dataSource.sort = this.sort;
                    this.dataSource.paginator = this.paginator;
                });

            });
        }
    }

    private submitData() {

        const nowEpoch =  (new Date('May 7, 2018 11:13:11')).getTime();

        const data = {
            lat: this.lat,
            lon: this.lon,
            start_time: nowEpoch
        }

        this._http.post(this.DATA_URL, data).subscribe( (response) => {
            console.log('RESULTS', response);
            this.result = JSON.parse(JSON.stringify(response));

            const DATA = [];

            // Every list
            for(let i = 0; i < this.result.length; ++i)
            {
                const tempData = {
                    name: this.result[i][0],
                    dist: this.result[i][1],
                    timeLeft: this.result[i][2],
                    lat: this.result[i][3][0],
                    lon: this.result[i][3][1],
                    rank: this.result[i][5],
                };
                DATA.push(tempData);
            }
            this.dataSource.data = (DATA);
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
        });
    }
    private selectRow(row) {
        this.markerSelected = true;
        this.newLat = +row.lat;
        this.newLon = +row.lon;
        this.centerlat = +this.newLat;
        this.centerlon = +this.newLon;
        this.selectedRowIndex = row.name;
        console.log('Selected row:', row);
    }
}
