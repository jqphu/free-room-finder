import { Component, OnInit } from '@angular/core';
import {FormsModule} from '@angular/forms';

@Component({
    selector: 'app-timepicker',
    templateUrl: './timepicker.component.html',
    styleUrls: ['./timepicker.component.css']
})

export class TimepickerComponent implements OnInit {
    public startDate = "13:30";
    public endDate = "14:30";

    constructor() { }

    ngOnInit() {
    }

}
