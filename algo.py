import numpy as np
from geopy import distance
import datetime
import csv
from Structures import Building
from Structures import Room
from Structures import Timetable
from Structures import get_buildings
import random


buildingToLatLon = {}

def get_buildings_dict():
    # Read in csv and store the long lat
    with open('buildingToLongLat.csv', 'r') as csv_read_file:
        reader = csv.reader(csv_read_file)
        for row in reader:
            #print(row)
            buildingToLatLon[row[0]] = (float(row[1]), float(row[2]))
    return buildingToLatLon

def get_free_rooms(epochStart, latLon) :


    validRooms = build_free_rooms_list(epochStart, latLon)

    validRooms = [v for v in validRooms if v[2] < 3 * 60 or random.random() < 0.05]

    validRooms = [[*x, 0] for x in validRooms]

    roomsByDist = sorted(validRooms, key = lambda x:x[1], reverse = True)
    roomsByTime = sorted(validRooms, key = lambda x:min(x[2], 3 * 60))

    #import pdb; pdb.set_trace()

    for i, (r1, r2) in enumerate(zip(roomsByDist, roomsByTime)) :
        r1[4] += i
        r2[4] += i


    retRooms = []
    for vr in validRooms :
        dist = '{:.2}km'.format(vr[1])
        if vr[2] > 60 * 3 :
            timeFree = '>3 Hours'
        else :
            h, m = divmod(vr[2], 60)
            timeFree = '{} Hrs, {} Mins'.format(int(h), int(m))
        retRooms.append((vr[0].fullName, dist, timeFree, vr[3], vr[4]))

    retRooms.sort(key = lambda x:x[-1], reverse = True)
    retRooms = [[*x, i] for i, x in enumerate(retRooms, 1)]
    return retRooms


def build_free_rooms_list(start_time, pos):
    listBuildings = get_buildings();
    start_datetime = datetime.datetime.fromtimestamp(start_time);

    # import pdb
    # pdb.set_trace()

    valid_rooms = []
    for building in listBuildings:
        for room in building.rooms:
            if not room.parsed:
                continue
            minsFree = room.get_time_free(start_datetime)
            try :
                dist = get_distance(room, pos)
            except :
                print('Distance find failed')
                continue
            if dist <= 5 and minsFree > 30:
                valid_rooms.append((room, dist, minsFree, buildingToLatLon[room.buildingCode]))
            # for bookings in room.timetable:
            #     if room.buildingC0ode in buildingToLatLon:
            #         dist = get_distance(room, pos)
            #             valid_rooms.append(room, dist)
            #             break;
    return valid_rooms

def get_distance(room, user_position):
    room_position = buildingToLatLon[room.buildingCode];
    dist = distance.distance(user_position,room_position).km
    return dist

if __name__ == '__main__' :
    get_buildings_dict()
    PNRLat = -33.890161899999
    PNRLon = 151.1922707
    formattedRooms = get_free_rooms(datetime.datetime(2018, 5, 9, 10).timestamp(), (PNRLat, PNRLon))
