import requests
from bs4 import BeautifulSoup
import re
import time
import Scraper
import ast
import sys
from datetime import datetime, timedelta

SURPRESS_DAY_ERRORS = True

class Building:

    def __init__(self, code):
        self.code = code
        self.roomVenueIds = []
        self.rooms = []

    def __repr__(self) :
        return '<Building: {}>'.format(self.code)

class Room:

    def __init__(self, name, buildingCode):
        self.name = name
        self.buildingCode = buildingCode
        self.timetable = []
        self.parsed = False

    def parse_timetable(self) :
        try :
            self.timetable = [Booking.from_scrape(t) for t in self.timetable]
            self.parsed = True
        except Exception as e :
            if not SURPRESS_DAY_ERRORS :
                print('Error parsing timetable for venue ', self.name)
                print(e)

    def __repr__(self) :
        return '<Room (Id:{})>'.format(self.name)

    def get_time_free(self, time) :
        if not self.parsed :
            return []

        havntEnded = [x for x in self.timetable if x.end > time]

        if len(havntEnded) > 0 :
            nearest = min(book.start - time for book in havntEnded)
            mins = nearest.days * 24 * 60 + nearest.seconds // 60
            if mins <= 0 :
                return 0
            return mins
        return 60 * 24



class Timetable:

    def __init__(self):
        pass

class Booking:

    def __init__(self, name) :
        self.name = name
        self.start = None
        self.end = None

    @classmethod
    def from_scrape(cls, dataTup) :

        book = cls(dataTup[1])
        day = dataTup[0]
        start, end = [get_time_obj(day, x.strip()) for x in dataTup[2].split(' - ')]
        book.start = start
        book.end = end
        return book

    def __repr__(self) :
        return '<Booking {}, ({}) - ({})>'.format(self.name if len(self.name) <= 10 else self.name[:10] + '...', self.start, self.end)

def get_time_obj(day, tstr) :
    cTime = datetime.now() - timedelta(days=datetime.now().weekday())
    lastMonday = datetime(cTime.year, cTime.month, cTime.day)
    weekDays = ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN']
    dayNum = weekDays.index(day)
    hours, mins = tstr.split(':')
    ret = lastMonday + timedelta(days = dayNum, hours=int(hours), minutes=int(mins))
    #print(ret)
    return ret

def make_buildings() :
    buildings = {}
    with open('venueToBuilding.csv') as inF:
        for line in inF:
            venueId, bCode = [x.strip() for x in line.split(',')]
            if bCode not in buildings :
                buildings[bCode] = Building(bCode)
            buildings[bCode].roomVenueIds.append(venueId)

    return buildings

def get_html_from_venueId(venueId) :
    url = 'https://web.timetable.usyd.edu.au/venuebookings/venueCalendar.jsp?venueId={}&mode=Bookings'.format(venueId)
    print(url)
    response = requests.get(url, verify = False)
    soup = BeautifulSoup(response.text, 'html.parser')
    return soup

def post_scrape_process(buildings) :
    for bCode in buildings :
        for room in buildings[bCode].rooms:
            room.parse_timetable()


def parse_log(buildings) :

    with open('timetableLog.txt', 'rt') as inF :
        lines = [x.strip() for x in inF]
        i = 0
        #import pdb; pdb.set_trace()
        while i < len(lines) and '###BCODE###' in lines[i] :
            i += 1
            bCode = lines[i]
            curBuilding = buildings[bCode]
            print('Reading:', bCode)
            i += 1
            while i < len(lines) and '###BCODE###' not in lines[i]:
                venueId = lines[i]
                curRoom = Room(venueId, bCode)
                curBuilding.rooms.append(curRoom)
                #print(venueId)
                i += 1
                while i < len(lines) and lines[i][0] == '(' :
                    booking = ast.literal_eval(lines[i])
                    curRoom.timetable.append(booking)
                    #print(booking)
                    i += 1






def scrape_timetables(buildings) :

    with open('timetableLog.txt', 'a') as o:
        i = 0
        for bCode in buildings:
            if len(buildings[bCode].rooms) > 0 :
                continue
            print('scraping ', bCode)
            print('###BCODE###', file = o)
            print(bCode, file = o)
            roomQ = Scraper.get_classes_for_venIds(buildings[bCode].roomVenueIds)

            #return list(roomQ.queue)

            for room in list(roomQ.queue) :
                print(room[0], file = o)
                for booking in room[1] :
                    print(booking, file = o)
            # i += 1
            #
            # if i > 1:
            #     return list(roomQ.queue)
            #for venueId in buildings[bCode].roomVenueIds:
                # soup = get_html_from_venueId(venueId)
                # tables = soup.find_all('table')
                # room = Room(venueId, bCode)
                # print(venueId, file = o)
                # for t in tables :
                #     #print(t.tr.th)
                #     if t.tr.th is not None and '<th>Booking #</th>' in str(t.tr.th):
                #         rows = t.find_all('tr')
                #         for row in rows[1:]:
                #             elems = row.find_all('td')
                #             bookingName = elems[1].a.text
                #             day = elems[2].text
                #             times = elems[3].text
                #             print(bookingName, day, times, file = o)
                #             book = Booking.from_scrape(bookingName, day, times)
                #             room.timetable.append(book)

            #     buildings[bCode].rooms.append(room)
            # return buildings

def scrape_room_names(buildings, useCache = True) :

    codeToNameDict = {}
    if useCache :
        with open('VenueIdToName.txt', 'rt') as inF :
            codeToNameDict = ast.literal_eval(inF.read())

    else :
        response = requests.get('https://web.timetable.usyd.edu.au/venuebookings/venueDetails.jsp', verify=False)


        soup = BeautifulSoup(response.text, 'html.parser')
        for option in soup.find_all('option'):
            venueCode = option.get('value')
            text = option.text
            name = text.split('(')[0].strip()
            codeToNameDict[venueCode] = name
            # buildingCode = text[(text.rfind('(') + 1):text.rfind('.')]
            # venueToBuilding[venueCode] = buildingCode

        with open('VenueIdToName.txt', 'wt') as outF :
            print(codeToNameDict, file = outF)

    for build in buildings :
        for room in build.rooms:
            try :
                room.fullName = codeToNameDict[room.name]
            except KeyError as ke :
                print('Venue ID {} not found'.format(room.name))
                print(ke)


def get_buildings() :
    buildings = make_buildings()
    parse_log(buildings)
    post_scrape_process(buildings)
    bList = list(buildings.values())
    scrape_room_names(bList)
    return bList

if __name__ == '__main__':
    bList = get_buildings()
    # buildings = make_buildings()
    #
    # parse_log(buildings)
    # roomsList = scrape_timetables(buildings)
    # post_scrape_process(buildings)


    # soup = get_html_from_venueId(1779)
    #
    # #import pdb; pdb.set_trace()
    # tables = soup.find_all('table')
    # for t in tables :
    #     print(t.tr.th)
    #     if t.tr.th is not None and '<th>Booking #</th>' in str(t.tr.th):
    #         rows = t.find_all('tr')
    #         for row in rows[1:]:
    #             elems = row.find_all('td')
    #             bookingName = elems[1].a.text
    #             day = elems[2].text
    #             times = elems[3].text
    #             book = Booking.from_scrape(bookingName, day, times)
    #             # print(booking, day, times)
