import requests
import csv
from bs4 import BeautifulSoup

def main():

    response = requests.get('https://web.timetable.usyd.edu.au/venuebookings/venueDetails.jsp', verify=False)

    venueToBuilding = {} 
    soup = BeautifulSoup(response.text, 'html.parser')
    for option in soup.find_all('option'):
        venueCode = int(option.get('value'))
        text = option.text
        buildingCode = text[(text.rfind('(') + 1):text.rfind('.')]
        venueToBuilding[venueCode] = buildingCode
        


    with open('test.csv', 'w') as csv_file:
        writer = csv.writer(csv_file)
        for key, value in venueToBuilding.items():
            print('Key is: ' + str(key))
            if len(value) != 3:
                response = requests.get('https://web.timetable.usyd.edu.au/venuebookings/venueDetails.jsp?vs=0&venueId=' +
                str(key), verify=False)
                soup = BeautifulSoup(response.text, 'html.parser')
                locationTable = soup.find_all('table')[-2]
                buildingCodeRow = locationTable.find_all('tr')[-1]
                buildingCode = buildingCodeRow.find_all('td')[-1].text
            writer.writerow([key, buildingCode])
def fillBroken():
    with open('test.csv', 'w') as csv_write_file:
        writer = csv.writer(csv_write_file)

        with open('venueToBuilding.csv', 'r') as csv_file:
            reader = csv.reader(csv_file)
            for row in reader:
                print(row)
                if len(row[1]) <= 4:
                    writer.writerow([row[0], row[1]])
                    continue


                response = requests.get('https://web.timetable.usyd.edu.au/venuebookings/venueDetails.jsp?vs=0&venueId=' +
                        str(row[0]), verify=False)
                soup = BeautifulSoup(response.text, 'html.parser')
                locationTable = soup.find_all('table')[-2]
                buildingCodeRow = locationTable.find_all('tr')[-1]
                buildingCode = buildingCodeRow.find_all('td')[-1].text
                if len(buildingCode) > 4:
                    continue
                writer.writerow([row[0], buildingCode])

if __name__ == "__main__":
    fillBroken()

