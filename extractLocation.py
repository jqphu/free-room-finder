import requests
import csv
import json
from bs4 import BeautifulSoup

def main():

    seen = {}
    with open('buildingToLongLat.csv', 'w') as csv_write_file:
        writer = csv.writer(csv_write_file)
        writer.writerow(['Building code', 'Latitude', 'Longitude'])
        with open('venueToBuilding.csv', 'r') as csv_read_file:
            reader = csv.reader(csv_read_file)	
            for row in reader:
                print(row)
                buildingCode = row[1]
                if buildingCode in seen:
                    continue
                seen[buildingCode] = True

                response = requests.get('https://sydney.edu.au/maps/embed/?building=' + str(buildingCode), verify=False)

                soup = BeautifulSoup(response.text, 'html.parser')
                try:
                    dataMap = soup.find('div', {'class':'usyd-app-maps-embedded'}).get('data-map')
                    longLat = json.loads(dataMap)['centre']
                    writer.writerow([buildingCode, longLat['lat'], longLat['lon']])
                except:
                    print('Bad result')
                    continue




    

    print('meme')


if __name__ == "__main__":
    main()


